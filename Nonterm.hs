{-# LANGUAGE DeriveDataTypeable #-}

import System.Environment
import Data.List as L
import Data.List.Split
import Data.List.Extra
import Data.Set as Set
import ARS
import Automata as A
import NormalForms
import Text.JSON.Generic


testRules :: [Rule String]
testRules = [(Rule "abc" "cba")]

testString = "( \n\n   Rules\n 0 1 -> 1 0,\n1 1 -> 2 2\n)"

parseRules :: [Char] -> [Rule String]
parseRules xs
           | L.head xs == '(' && L.last xs == ')' = do
                                                    let l = trim (L.init (L.tail xs))
                                                    case l of
                                                         ('R':'U':'L':'E':'S':xs) -> parseRules' (trim xs) []
                                                         other -> error "wrong data1"
           | otherwise = error "wrong data2"
           where parseRules' :: [Char] -> [Rule String] -> [Rule String]
                 parseRules' xs rules = let (rule,rest) = parseRule xs in case rest of
                                                                          [] -> (rules ++ [rule])
                                                                          other -> parseRules' rest (rules ++ [rule])

parseRule :: [Char] -> (Rule String, [Char])
parseRule [] = error "wrong data3"
parseRule xs = (parseRight . parseLeft) xs

parseLeft :: [Char] -> ([Char], [Char])
parseLeft xs = parseLeft' xs []
          where parseLeft' :: [Char] -> [Char] -> ([Char],[Char])
                parseLeft' [] xs = error "wrong data4"
                parseLeft' ('-':'>':xs) res = (res,xs)
                parseLeft' ('-':xs) _ = error "wrong data5"
                parseLeft' xs res = let l = (trim xs) in parseLeft' (trim $ L.tail l) (res ++ [L.head l])

parseRight :: ([Char], [Char]) -> (Rule String, [Char])
parseRight (lhs,ys) = let (rhs, rest) = parseRight' ys [] in ((Rule lhs rhs), rest)
           where parseRight' :: [Char] -> [Char] -> ([Char],[Char])
                 parseRight' [] res = case res of
                                      [] -> error "wrong data6"
                                      other -> (res, [])
                 parseRight' (',':xs) res = case res of
                                            [] -> error "wrong data7"
                                            other -> (res, xs)
                 parseRight' xs res = let l = (trim xs) in parseRight' (L.tail l) (res ++ [L.head l])

main :: IO ()
main = do
     inp <- getLine
     print $ encode $ toJSON $ makeNonNFShifted $ parseRules inp
