{-# LANGUAGE DeriveDataTypeable #-}

module Automata where

import Data.List as L
import Data.HashMap.Strict as H
import Data.Hashable
import Data.Typeable
import Data.Data

data FSM a = FSM {
                   states :: [a],
                   sigma :: [Char],
                   delta :: [(a, Char, a)],
                   init :: a,
                   finals :: [a]
                 } deriving (Show, Eq, Typeable, Data)

union :: Eq a => (FSM a) -> (FSM a) -> FSM (a, a)
union (FSM st1 sig1 del1 i1 f1) (FSM st2 sig2 del2 i2 f2)
      | sig1 /= sig2 = error "\nError: can not unify two automata due to different alphabets\n"
      | otherwise = let stp = [(a,b) | a <- st1, b <- st2]
                        sigp = sig1
                        delp = prodTrans [(m,n) | m <- del1, n <- del2 ]
                        ip = (i1,i2)
                        fp = [(m,n) | m <- st1, n <- st2, elem m f1 || elem n f2]
                    in FSM stp sigp delp ip fp
      where
                prodTrans :: [((a, Char, a), (a, Char, a))] -> [((a,a), Char, (a,a))]
                prodTrans [] = []
                prodTrans (((p1,c1,q1),(p2,c2,q2)):xs)
                          | c1 == c2 = (((p1,p2),c1,(q1,q2)): prodTrans xs)
                          | otherwise = prodTrans xs

simplify :: (Hashable a, Eq a) => (FSM (a,a)) -> FSM Int
simplify (FSM st a t i f) = let smap = simpleMap st empty in
                                FSM (L.map (\x -> updState x smap) st)
                                    a
                                    (L.map (\x -> let (p,c,q) = x in (updState p smap, c, updState q smap)) t)
                                    (updState i smap)
                                    (L.map (\x -> updState x smap) f)

fixInd :: (Hashable a, Eq a) => (FSM (a)) -> FSM Int
fixInd (FSM st a t i f) = let smap = simpleMap st empty in
                                FSM (L.map (\x -> updState x smap) st)
                                    a
                                    (L.map (\x -> let (p,c,q) = x in (updState p smap, c, updState q smap)) t)
                                    (updState i smap)
                                    (L.map (\x -> updState x smap) f)

simpleMap :: (Hashable a, Eq a) => [a] -> (HashMap a Int) -> (HashMap a Int)
simpleMap [] m = m
simpleMap (x:xs) m = simpleMap xs (H.insert x (size m) m)

updState :: (Hashable a, Eq a) => a -> (HashMap a Int) -> Int
updState x m = case H.lookup x m of
               Just a -> a
               Nothing -> error "\nError: can not simplify the FSM\n"
