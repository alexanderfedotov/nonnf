module NormalForms where

import Data.List as L
import Data.List.Split as Split
import Data.Set as Set
import Data.HashMap.Strict as H
import ARS
import Automata as A




getSigma :: [Rule String] -> [Char]
getSigma rls = Set.toList $ getSigma' rls
         where getSigma' :: [Rule String] -> Set Char
               getSigma' [] = Set.empty
               getSigma' (x:xs) = Set.union (getSigma' xs) (Set.union (Set.fromList $ rhs x) (Set.fromList $ lhs x))

getLhs :: [Rule String] -> [String]
getLhs xs = [ lhs x | x <- xs ]

getShifted :: [Rule String] -> [String]
getShifted rls = Set.toList $ getShifted' $ getLhs rls
           where getShifted' :: [String] -> Set String
                 getShifted' [] = Set.empty
                 getShifted' (x:xs) = Set.union (Set.fromList (if (length x > 1)
                                                               then [(drop i x) ++ "$" ++ (take i x) | i <- [1..(length x - 1)]]
                                                               else [x]))
                                                (getShifted' xs)

getPrefs :: String -> [String]
getPrefs str = [take i str | i <- [1..(length str)]]

setA :: [Rule String] -> [String]
setA rls = Set.toList $ setA' rls
     where setA' :: [Rule String] -> Set String
           setA' [] = Set.singleton ""
           setA' (x:xs) = Set.union (Set.fromList $ getPrefs $ lhs x) (setA' xs)

setB :: String -> [String]
setB rls = Set.toList (Set.filter (\a -> notMember (a ++ "$") (setB' rls)) (setB' rls))
     where setB' :: String -> Set String
           setB' x = Set.union (Set.fromList $ getPrefs x) (Set.union (Set.singleton "@") (Set.singleton ""))

addToStateMap :: [String] -> (HashMap String Int) -> HashMap String Int
addToStateMap [] m = m
addToStateMap (x:xs) m = addToStateMap xs (H.insert x (H.size m) m)

stateMapA :: [Rule String] -> HashMap String Int
stateMapA rls = addToStateMap (setA rls) H.empty

stateMapB :: String -> HashMap String Int
stateMapB rls = addToStateMap (setB rls) H.empty

computeTrans :: String -> [Rule String] -> (HashMap String Int) -> (String -> [Rule String] -> String) -> Int
computeTrans s rls m f = case (H.lookup (f s rls) $ m) of
                       Just x -> x
                       Nothing -> error ("\nError: Can not build an NF automaton: " ++ (f s rls))

allTrans :: [Rule String] -> (HashMap String Int) -> [String] -> (String -> [Rule String] -> String) -> [(Int, Char, Int)]
allTrans rls m [] f = []
allTrans rls m (x:xs) f = [(getId x m, c, getId (f ([c] ++ x) rls) m) | c <- getSigma rls] ++ (allTrans rls m xs f)

getId :: String -> (HashMap String Int) -> Int
getId s m = case H.lookup s m of
            Just x -> x
            Nothing -> error "\nError: Can not build an NF automaton\n"


matchA :: String -> [Rule String] -> String
matchA s rls
       | elem (take (length s - 1) s) (getLhs rls) = (take (length s - 1) s)
       | elem s $ setA rls = s
       | (length s) > 1 = matchA (tail s) rls
       | otherwise = ""


matchB :: String -> String -> String
matchB s rls
       | elem s $ setB rls = s
       | elem (s ++ "$") $ setB rls = s ++ "$"
       | elem '$' s = let spl = Split.splitOn "$" s in case (length spl) of
                                                       2 -> (matchB ((head spl) ++ "$" ++ (drop 1 $ spl !! 1)) rls)
                                                       other -> error "\nError: Can not build an NF automaton\n"
       | otherwise = "@"



makeTrans :: [Char] -> [String] -> [Rule String] -> (HashMap String Int) -> (String -> [Rule String] -> String) -> [(Int, Char, Int)]
makeTrans sigma l rls m f = makeTrans' l []
          where makeTrans' :: [String] -> [(Int, Char, Int)] -> [(Int, Char, Int)]
                makeTrans' [] res = res
                makeTrans' (x:xs) res = [(getId x m, a, getId (f (x ++ [a]) rls) m) | a <- sigma] ++ (makeTrans' xs res)

makeTransB :: [Char] -> [String] -> String -> (HashMap String Int) -> (String -> String -> String) -> [(Int, Char, Int)]
makeTransB sigma l rls m f = makeTransB' l []
          where makeTransB' :: [String] -> [(Int, Char, Int)] -> [(Int, Char, Int)]
                makeTransB' [] res = res
                makeTransB' (x:xs) res = [(getId x m, a, getId (f (x ++ [a]) rls) m) | a <- sigma] ++ (makeTransB' xs res)

makeFinals :: [String] -> [String] -> (HashMap String Int) -> [Int]
makeFinals l fin m = [getId x m | x <- l, elem x fin]

makeA :: [Rule String] -> FSM Int
makeA rls = let q = L.map (\x -> getId x (stateMapA rls)) (setA rls)
                sig = getSigma rls
                del = makeTrans (getSigma rls) (setA rls) rls (stateMapA rls) matchA
                init = getId "" (stateMapA rls)
                fin = makeFinals (setA rls) (getLhs rls) (stateMapA rls)
            in FSM q sig del init fin

makeB :: [Char] -> String -> FSM Int
makeB sig rls = let q = L.map (\x -> getId x (stateMapB rls)) (setB rls)
                    --sig = getSigma rls
                    del = makeTransB sig (setB rls) rls (stateMapB rls) matchB
                    init = getId "" (stateMapB rls)
                    fin = makeFinals (setB rls) [rls] (stateMapB rls)
                in FSM q sig del init fin

makeNonNFShifted :: [Rule String] -> FSM Int
makeNonNFShifted rls = makeNonNFShifted' (getShifted rls) (makeA rls)
                 where makeNonNFShifted' :: [String] -> (FSM Int) -> FSM Int
                       makeNonNFShifted' [] res = res
                       makeNonNFShifted' (x:xs) res = makeNonNFShifted' xs (fixInd (noUnreach (reduce rls (simplify (A.union res (makeB (getSigma rls) x))))))


{- Minimization -}

getDestDFA :: Int -> Char -> [(Int, Char, Int)] -> Int
getDestDFA s a [] = error "\nCan not determine outgoing transition\n"
getDestDFA s a ((p,c,q):xs)
           | s == p && a == c = q
           | otherwise = getDestDFA s a xs

data MinPair = MinPair { pair :: (Int, Int), isDist :: Bool } deriving (Show, Eq)

pairsForMin :: [Int] -> [Int] -> [MinPair]
pairsForMin st fin = [MinPair (x, y) ((elem x fin && not (elem y fin)) || (not (elem x fin)) && elem y fin) | x <- st, y <- st, x < y ]

checkDist :: Int -> Int -> [MinPair] -> Bool
checkDist p q [] = error ("\nCan not check if the states are distinguishable:" ++ (show (p,q)))
checkDist p q ((MinPair (m,n) b):xs)
          | p == q = False
          | p == m && q == n || p == n && q == m = b
          | otherwise = checkDist p q xs

checkSuccDist :: Int -> Int -> [Char] -> [MinPair] -> [(Int, Char, Int)] -> Bool
checkSuccDist p q c pairs t = L.foldl (||) False [checkDist (getDestDFA p a t) (getDestDFA q a t) pairs | a <- c]

markPairs :: [Char] -> [MinPair] -> [(Int, Char, Int)] -> [MinPair]
markPairs sig p t = [let (MinPair (m,n) f) = q in if f == False then (MinPair (m,n) (checkSuccDist m n sig p t)) else (MinPair (m,n) True) | q <- p]

markAll :: [Char] -> [MinPair] -> [(Int, Char, Int)] -> [MinPair]
markAll sig p t
        | p == (markPairs sig p t) = p
        | otherwise = markAll sig (markPairs sig p t) t

toMerge :: [MinPair] -> [([Int],Int)]
toMerge p = toMerge' p []
        where toMerge' :: [MinPair] -> [([Int],Int)] -> [([Int],Int)]
              toMerge' [] res = res
              toMerge' ((MinPair (m,n) True):xs) res = toMerge' xs res
              toMerge' ((MinPair (m,n) False):xs) res = toMerge' xs (addToMerges m n res)

addToMerges :: Int -> Int -> [([Int],Int)] -> [([Int],Int)]
addToMerges a b [] = [([b],a)]
addToMerges a b ((m,n):xs)
            | a == n = ((Set.toList (Set.fromList (m ++ [b])),n):xs)
            | b == n = ((Set.toList (Set.fromList (m ++ [a])),n):xs)
            | elem a m || elem b m = ((Set.toList (Set.fromList (m ++ [a] ++ [b])),n):xs)
            | otherwise = ((m,n):addToMerges a b xs)

doMerge :: Int -> [([Int],Int)] -> Int
doMerge x [] = x
doMerge x [(m,n)]
        | elem x m = n
        | otherwise = x
doMerge x ((m,n):ys)
        | elem x m = n
        | otherwise = doMerge x ys

reduce :: [Rule String] -> (FSM Int) -> FSM Int
reduce rls (FSM q sig del i fin) = let merges = (toMerge (markAll (getSigma rls) (pairsForMin q fin) del)) in
       FSM (Set.toList (Set.fromList (L.map (\x -> doMerge x merges) q)))
           sig
           (Set.toList (Set.fromList (L.map (\x -> let (m,c,n) = x in (doMerge m merges, c, doMerge n merges)) del)))
           i
           (Set.toList (Set.fromList (L.map (\x -> doMerge x merges) fin)))

isReachable :: Int -> [Char] -> [(Int, Char, Int)] -> Bool
isReachable q sig [] = False
isReachable q sig xs = isReachable' 0 q sig xs []
            where isReachable' :: Int -> Int -> [Char] -> [(Int, Char, Int)] -> [Int] -> Bool
                  isReachable' p q sig t v
                               | p == q = True
                               | p /= q && v == (Set.toList (Set.fromList (v ++ (addVisited p sig t)))) = False
                               | otherwise = L.foldl (||) False [isReachable' (getDestDFA p a t) q sig t (Set.toList (Set.fromList (v ++ (addVisited p sig t)))) | a <- sig]

addVisited :: Int -> [Char] -> [(Int, Char, Int)] -> [Int]
addVisited q sig t = [(getDestDFA q a t) | a <- sig]

getUnreachable :: [Int] -> [Char] -> [(Int, Char, Int)] -> [Int]
getUnreachable [] sig t = []
getUnreachable (x:xs) sig t
               | not (isReachable x sig t) = (x: getUnreachable xs sig t)
               | otherwise = getUnreachable xs sig t

noUnreach :: (FSM Int) -> FSM Int
noUnreach (FSM q sig t i fin) = let q' = (L.filter (\x -> isReachable x sig t) q)
                                    t' = (L.filter (\x -> let (m,c,n) = x in (isReachable m sig t) && (isReachable m sig t)) t)
                                    fin' = (L.filter (\x -> isReachable x sig t) fin)
                                in FSM q' sig t' i fin'
