module ARS where

data Rule a = Rule { lhs :: a, rhs :: a } deriving Show
